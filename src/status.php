<?php

namespace attics\Usenet\yenc;

/**
 * Class status
 * @see http://www.yenc.org/yenc-draft.1.3.txt
 * @package attics\Usenet\yenc
 */
class status
{
    /**
     * Line length
     * @var int
     */
    public $line;

    /**
     * Filename like binary.rar
     * @var string
     */
    public $name;

    /**
     * Size of decoded binary file. Binary means file, which is consists of all parts
     * @var integer
     */
    public $size;

    /**
     * @var part
     */
    public $part;

    /**
     * representing the CRC32 of the entire encoded binary.
     * @var string
     */
    public $crc32;

    /**
     * total parts
     * @var integer
     */
    public $total;

    /**
     * status constructor.
     * @param array $ybegin
     * @param array $ypart
     * @param array $yend
     */
    public function __construct($ybegin,$ypart,$yend)
    {
        if(!empty($ybegin['line'])){
            $this->line = intval($ybegin['line']);
        }

        if(!empty($ybegin['size'])){
            $this->size = intval($ybegin['size']);
        }

        if(!empty($ybegin['name'])){
            $this->name = trim($ybegin['name']);
        }

        if(!empty($yend['crc32'])){
            $this->crc32 = trim(strtolower($yend['crc32']));
        }

        $this->part = new part($ypart,$yend);
    }
}