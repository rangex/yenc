<?php
namespace attics\Usenet\yenc;


class part
{
    /**
     * Current part number
     * =yend .. part=10 ...
     * @var integer
     */
    public $number;

    /**
     * Start position in binary
     * =ypart begin=400001
     * @var integer
     */
    public $begin;

    /**
     * End position in binary
     * =ypart .. end=500000
     * @var integer
     */
    public $end;

    /**
     * Current part size in bytes
     * =yend size=100000
     * @var integer
     */
    public $size;

    /**
     * "pcrc32" keyword representing the CRC32 of the preceeding encoded part.
     * =yend .. pcrc32=abcdef12
     * @var string
     */
    public $pcrc32;

    /**
     * part constructor.
     * @param array $ypart
     * @param array $yend
     */
    public function __construct($ypart, $yend)
    {
        if (!empty($yend['size'])) {
            $this->size = intval($yend['size']);
        }
        if (!empty($yend['part'])) {
            $this->number = intval($yend['part']);
        }
        if (!empty($yend['pcrc32'])) {
            $this->pcrc32 = trim(strtolower($yend['pcrc32']));
        }
        if (!empty($ypart['begin'])) {
            $this->begin = intval($ypart['begin']);
        }
        if (!empty($ypart['end'])) {
            $this->end = intval($ypart['end']);
        }
    }
}
