<?php

namespace attics\Usenet\yenc;

class Exception extends \Exception
{
    const YENC_NOT_FOUND = 100;
    const HEADER_SIZE_NOT_FOUND = 101;
    const FOOTER_CRC_NOT_FOUND = 102;
    const SIZE_DIFFERS = 103;
    const CRC_DIFFERS = 104;
    const BEGIN_NOT_FOUND = 105;
    const PART_NOT_FOUND = 106;
    const END_NOT_FOUND = 107;

    private $_errors = [
        self::YENC_NOT_FOUND => 'yEnc meta data not found',
        self::HEADER_SIZE_NOT_FOUND => 'Unable to find size in header',
        self::SIZE_DIFFERS => 'Decoded size differs from meta size',
        self::FOOTER_CRC_NOT_FOUND => 'Unable to find CRC in yEnc footer',
        self::CRC_DIFFERS => 'CRC32 checksums do not match',
        self::BEGIN_NOT_FOUND => '=ybegin not found',
        self::PART_NOT_FOUND => '=ypart not found',
        self::END_NOT_FOUND => '=yend not found',
    ];

    public function __construct($err_code = null, $extra=null)
    {
        if (empty($err_code) || !array_key_exists($err_code, $this->_errors)) {
            return parent::__construct('Unknown error', $err_code);
        }

        $message = $this->_errors[$err_code];
        if(!empty($extra)){
            $message.=' -> '.$extra;
        }
        return parent::__construct($message, $err_code);
    }
}